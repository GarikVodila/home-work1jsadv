class Employee {
    constructor ( name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name () {
        return this._name;
    }

    set name (value) {
        this._name = value;
    }

    get age () {
        return this._age;
    }

    set age (value) {
        this._age = value;
    }

    get salary () {
        return this._salary;
    }

    set salary(value) {
        this._salary = value;
    }
}

class Programmer extends Employee {
    constructor ( name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    get salary () {
        return this._+salary * 3;
    }

    get lang () {
        return this._lang;
    }

    set lang (value) {
        this._lang = value;
    }
}

const Programmer1 = new Programmer ('Vlad', 16, 600, 'Ukr')
const Programmer2 = new Programmer ('Lari', 20, 1600, 'Eng')

console.log(Programmer1);
console.log(Programmer2);